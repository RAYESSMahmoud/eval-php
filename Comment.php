<?php

class Comment{
	private $id_comment ;
	private $nom_inter;
	private $comment;
	private $id_art;


	
	public function getId() : int
	{
		return $this->id_comment;
	}

	public function setNomInter(string $n) : self
	{
		$this->nom_inter = $n;
		return $this;
	}
	public function getNomInter() : string
	{
		return $this->nom_inter;
	}

	public function setComment(?string $d) : self
	{
		$this->comment = $d;
		return $this;
	}
	public function getComment() : ?string
	{
		return $this->comment;
	}

	public function setIdArt(?string $ia) : self
	{
		$this->id_art = $ia;
		return $this;
	}
	public function getIdArt() : ?string
	{
		return $this->id_art;
	}
}