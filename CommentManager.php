<?php

require_once 'conn_bdd.php';
require_once 'Comment.php';

class CommentManager extends Comment{
		public static function findAllByArt(int $id_art){
		$sql = 'SELECT * FROM comment WHERE id_art = :id_art';

		$bdd = new CONN_BDD();
		$co = $bdd->connexion();
		$req = $co->prepare($sql);
		$req->execute([
			'id_art' => $id_art
		]);

		return $req->fetchAll(PDO::FETCH_CLASS, 'Comment');	
	}

	public function save(){
		$sql = 'INSERT INTO comment(nom_inter, comment, id_art) VALUES (:n, :c, :id_art)';

		$bdd = new CONN_BDD();
		$co = $bdd->connexion();
		$req = $co->prepare($sql);
		$req->execute([
			'n' => $this->getNomInter(),
			'c' => $this->getComment(),
			'id_art' => $this->getIdArt()
		]);

		return $req->rowCount();
	}

	

}