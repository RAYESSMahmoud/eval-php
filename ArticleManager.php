<?php

require_once 'conn_bdd.php';
require_once 'Article.php';

class ArticleManager extends Art{
	public static function findAll(){
		$sql = 'SELECT * FROM art';

		$bdd = new CONN_BDD();
		$co = $bdd->connexion();
		$req = $co->prepare($sql);
		$req->execute();

		return $req->fetchAll(PDO::FETCH_CLASS, 'Art');
	}

	public function save(){
		$sql = 'INSERT INTO art(nom_art, description_art, id_cat) VALUES (:n, :d, :id_cat)';

		$bdd = new CONN_BDD();
		$co = $bdd->connexion();
		$req = $co->prepare($sql);
		$req->execute([
			'n' => $this->getNom(),
			'd' => $this->getDescription(),
			'id_cat' => $this->getIdCat()
		]);

		return $req->rowCount();
	}

	public static function delete(int $id_art){
		$sql = 'DELETE FROM art WHERE id_art = :id_art';

		$bdd = new CONN_BDD();
		$co = $bdd->connexion();
		$req = $co->prepare($sql);
		$req->execute([
			'id_art' => $id_art
		]);

		return $req->rowCount();
	}

	public static function findOneById(int $id_art){
		$sql = 'SELECT * FROM art WHERE id_art = :id_art';

		$bdd = new CONN_BDD();
		$co = $bdd->connexion();
		$req = $co->prepare($sql);
		$req->execute([
			'id_art' => $id_art
		]);

		return $req->fetchAll(PDO::FETCH_CLASS, 'Art');
	}
	
	public function update(int $id_art){
		$sql = 'UPDATE art SET nom_art = :n, description_art = :d, id_cat = :id_cat WHERE id_art = :id_art';

		$bdd = new CONN_BDD();
		$co = $bdd->connexion();
		$req = $co->prepare($sql);
		$req->execute([
			'n' => $this->getNom(),
			'd' => $this->getDescription(),
			'id_cat' => $this->getIdCat(),
			'id_art' => $id_art
		]);

		return $req->rowCount();
	}

	public static function findAllByCat(int $id_cat){
		$sql = 'SELECT * FROM art WHERE id_cat = :id_cat';

		$bdd = new CONN_BDD();
		$co = $bdd->connexion();
		$req = $co->prepare($sql);
		$req->execute([
			'id_cat' => $id_cat
		]);

		return $req->fetchAll(PDO::FETCH_CLASS, 'Art');
	}

	
	

}