<?php

require_once 'CategorieManager.php';

if(!isset($_POST['nom_cat']) || empty($_POST['nom_cat'])){
	echo 'Veuillez renseigner un nom de Catégorie';
}
if(!isset($_POST['description_cat']) || empty($_POST['description_cat'])){
	echo 'Veuillez renseigner la description de catégorie';
}
else{
	// Je ne vérifie pas la description car elle est autorisée à être vide en base

	// Création de l'objet
	$Manage_cat = new CategorieManager();
	$Manage_cat->setNom($_POST['nom_cat'])
		->setDescription($_POST['description_cat']);
	// Sauvegarde
	if($Manage_cat->update($_POST['id_cat']) > 0){
		header('Location:save.php'); 
	}
	else{
		echo "<p>Une erreur est survenue</p>";
	}
}

