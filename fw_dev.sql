-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : Dim 12 sep. 2021 à 20:08
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `fw_dev`
--

-- --------------------------------------------------------

--
-- Structure de la table `art`
--

DROP TABLE IF EXISTS `art`;
CREATE TABLE IF NOT EXISTS `art` (
  `id_art` int(11) NOT NULL AUTO_INCREMENT,
  `nom_art` varchar(255) NOT NULL,
  `description_art` text,
  `id_cat` int(11) NOT NULL,
  PRIMARY KEY (`id_art`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `art`
--

INSERT INTO `art` (`id_art`, `nom_art`, `description_art`, `id_cat`) VALUES
(10, 'Angular', 'Angular compte certainement parmi les cadres JavaScript les plus populaires et les plus utilisÃ©s pour crÃ©er des applications front. Bien quâ€™Angular lui-mÃªme ne soit pas nouveau et existe depuis un certain temps, la derniÃ¨re version dâ€™Angular, Angular 2, est trÃ¨s diffÃ©rente de la version prÃ©cÃ©dente. Elle est en effet dotÃ©e dâ€™une foule de nouveaux outils et de nouvelles fonctionnalitÃ©s.', 16),
(11, 'ReactJS', 'Câ€™est un framework qui est recommandÃ© par les entreprises proposant des solutions de dÃ©veloppement JavaScript. Il sâ€™agit dâ€™un framework de dÃ©veloppement web convivial. React peut Ãªtre utilisÃ© pour crÃ©er une interface graphique front et propose un modÃ¨le qui convient parfaitement Ã  tous les dÃ©veloppeurs web.', 16),
(12, 'Symfony', 'Symfony est un ensemble de composants PHP ainsi qu\'un framework MVC libre Ã©crit en PHP. Il fournit des fonctionnalitÃ©s modulables et adaptables qui permettent de faciliter et dâ€™accÃ©lÃ©rer le dÃ©veloppement d\'un site web.', 15);

-- --------------------------------------------------------

--
-- Structure de la table `cat`
--

DROP TABLE IF EXISTS `cat`;
CREATE TABLE IF NOT EXISTS `cat` (
  `id_cat` int(11) NOT NULL AUTO_INCREMENT,
  `nom_cat` varchar(255) NOT NULL,
  `description_cat` text,
  PRIMARY KEY (`id_cat`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `cat`
--

INSERT INTO `cat` (`id_cat`, `nom_cat`, `description_cat`) VALUES
(16, 'JavaScript', 'JavaScript est un langage de programmation de scripts principalement employÃ© dans les pages web interactives et Ã  ce titre est une partie essentielle des applications web. Avec les technologies HTML et CSS, JavaScript est parfois considÃ©rÃ© comme l\'une des technologies cÅ“ur du World Wide Web'),
(15, 'PHP', 'PHP: Hypertext Preprocessor, plus connu sous son sigle PHP, est un langage de programmation libre, principalement utilisÃ© pour produire des pages Web dynamiques via un serveur HTTP, mais pouvant Ã©galement fonctionner comme n\'importe quel langage interprÃ©tÃ© de faÃ§on locale. PHP est un langage impÃ©ratif orientÃ© objet');

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `id_comment` int(11) NOT NULL AUTO_INCREMENT,
  `nom_inter` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `id_art` int(11) NOT NULL,
  PRIMARY KEY (`id_comment`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `comment`
--

INSERT INTO `comment` (`id_comment`, `nom_inter`, `comment`, `id_art`) VALUES
(7, 'Codeur 1', 'Angular est lâ€™un des frameworks JS les plus performants et robustes. Ses mises Ã  jour frÃ©quentes dÃ©montrent lâ€™activitÃ© incessante de sa grande communautÃ©, et la capacitÃ© dâ€™adaptation dâ€™Angular.', 10),
(8, 'Codeur 2', 'Lâ€™apprentissage dâ€™Angular est long et ardu, ce qui peut dÃ©courager dans un premier temps, surtout lors de premiers dÃ©veloppement en intÃ©grant ce type de frameworks.', 10),
(9, 'Codeur 1', 'Le principal avantage de React est quâ€™il peut aider Ã  dÃ©velopper un composant qui sera rÃ©utilisÃ© encore et encore. Cela simplifie donc dâ€™autant le processus de dÃ©veloppement web.', 11),
(10, 'Codeur 2', 'la crÃ©ation est simplifiÃ©e grÃ¢ce Ã  sa flexibilitÃ© et son API qui permettent une utilisation facile des composants. Ã€ noter aussi que ReactJS travaille avec un DOM virtuel, permettant de gagner en performance et en rapiditÃ©.', 11),
(11, 'easypartner 1', 'Symfony est incontestablement un des frameworks PHP les plus apprÃ©ciÃ©s. Sa trÃ¨s importante communautÃ© rend les choses plus faciles en cas de difficultÃ© et il est toujours particuliÃ¨rement agrÃ©able de ne pas se sentir seul face Ã  son Ã©cran. La documentation est Ã©galement trÃ¨s fournie et accompagne les dÃ©veloppeurs, qui se sentent gÃ©nÃ©ralement trÃ¨s Ã  lâ€™aise en dÃ©veloppement en Symfony.', 12),
(12, 'easypartner 2', 'Symfony est particuliÃ¨rement apprÃ©ciÃ© pour sa modularitÃ©. Le framework offre de vastes possibilitÃ©s de fonctions pour crÃ©er de nombreux projets et applications Ã  la fois stables et performantes. Le dÃ©veloppement des API REST y est trÃ¨s robuste et Symfony est donc un framework idÃ©al pour les applications professionnelles.', 12);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
