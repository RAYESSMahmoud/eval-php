<?php

require_once 'conn_bdd.php';
require_once 'Categorie.php';

class CategorieManager extends Cat{
	public static function findAll(){
		$sql = 'SELECT * FROM cat';

		$bdd = new CONN_BDD();
		$co = $bdd->connexion();
		$req = $co->prepare($sql);
		$req->execute();

		return $req->fetchAll(PDO::FETCH_CLASS, 'Cat');
	}

	public function save(){
		$sql = 'INSERT INTO cat(nom_cat, description_cat) VALUES (:n, :d)';

		$bdd = new CONN_BDD();
		$co = $bdd->connexion();
		$req = $co->prepare($sql);
		$req->execute([
			'n' => $this->getNom(),
			'd' => $this->getDescription()
		]);

		return $req->rowCount();
	}

	public static function delete(int $id_cat){
		$sql = 'DELETE FROM cat WHERE id_cat = :id_cat';

		$bdd = new CONN_BDD();
		$co = $bdd->connexion();
		$req = $co->prepare($sql);
		$req->execute([
			'id_cat' => $id_cat
		]);

		return $req->rowCount();
	}

	public static function findOneById(int $id_cat){
		$sql = 'SELECT * FROM cat WHERE id_cat = :id_cat';

		$bdd = new CONN_BDD();
		$co = $bdd->connexion();
		$req = $co->prepare($sql);
		$req->execute([
			'id_cat' => $id_cat
		]);

		return $req->fetchAll(PDO::FETCH_CLASS, 'Cat');
	}
	
	public function update(int $id_cat){
		$sql = 'UPDATE cat SET nom_cat = :n, description_cat = :d WHERE id_cat = :id_cat';

		$bdd = new CONN_BDD();
		$co = $bdd->connexion();
		$req = $co->prepare($sql);
		$req->execute([
			'n' => $this->getNom(),
			'd' => $this->getDescription(),
			'id_cat' => $id_cat
		]);

		return $req->rowCount();
	}

}