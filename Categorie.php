<?php

class Cat{
	private $id_cat;
	private $nom_cat;
	private $description_cat;
	
	public function getId() : int
	{
		return $this->id_cat;
	}

	public function setNom(string $n) : self
	{
		$this->nom_cat = $n;
		return $this;
	}
	public function getNom() : string
	{
		return $this->nom_cat;
	}

	public function setDescription(?string $d) : self
	{
		$this->description_cat = $d;
		return $this;
	}
	public function getDescription() : ?string
	{
		return $this->description_cat;
	}
}