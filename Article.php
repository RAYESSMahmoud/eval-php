<?php

class Art{
	private $id_art;
	private $nom_art;
	private $description_art;
	private $id_cat;


	
	public function getId() : int
	{
		return $this->id_art;
	}

	public function setNom(string $n) : self
	{
		$this->nom_art = $n;
		return $this;
	}
	public function getNom() : string
	{
		return $this->nom_art;
	}

	public function setDescription(?string $d) : self
	{
		$this->description_art = $d;
		return $this;
	}
	public function getDescription() : ?string
	{
		return $this->description_art;
	}

	public function setIdCat(?string $ic) : self
	{
		$this->id_cat = $ic;
		return $this;
	}
	public function getIdCat() : ?string
	{
		return $this->id_cat;
	}
}
