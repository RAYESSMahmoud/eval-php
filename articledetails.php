<?php
if(isset($_GET['id_art'])){
require_once 'ArticleManager.php';
$Manage_art = new ArticleManager();
$article = ArticleManager::findOneById($_GET['id_art'])[0];




  require_once 'CommentManager.php';
  $Manage_cmt = new CommentManager();
  $cmts = CommentManager::findAllByArt($_GET['id_art']);
}
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>Pricing example · Bootstrap v5.1</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/pricing/">

    

    <!-- Bootstrap core CSS -->
<link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="pricing.css" rel="stylesheet">
  </head>
  <body>
    
<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
  <symbol id="check" viewBox="0 0 16 16">
    <title>Check</title>
    <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z"/>
  </symbol>
</svg>

<div class="container py-3">
<header>
    <?php include("nav.html") ?>
  </header>

  <main>
    <div class="row row-cols-1 row-cols-md-12 mb-3 text-center">


      <div class="col">
        <div class="card mb-8 rounded-3 shadow-sm">
          <div class="card-header py-3">
            <h4 class="my-0 fw-normal"><?= $article->getNom() ?> </h4>
          </div>
          <div class="card-body">
            <small class="text-muted fw-light">
              <?= $article->getDescription() ?>
            </small>
          </div>
        </div>
      </div>


      <div class="col">
        <div class="card mb-8 rounded-3 shadow-sm">
          <div class="card-header py-3">
            <hZ class="my-0 fw-normal">Commentaires </h2>
          </div>
          <div class="card-body">


          <center>
            <form class="form-horizontal" action="insert_cmt.php" method="POST">
            <fieldset>


            <!-- Text input-->
            <div class="form-group" hidden> 
              <div class="col-md-5">
              <input id="textinput" name="id_art" value="<?= $_GET['id_art'] ?>" type="text" placeholder="" class="form-control input-md">
              </div>
            </div>
            <br>
            <!-- Text input-->
            <div class="form-group"> 
              <div class="col-md-5">
              <input id="textinput" name="nom_inter" type="text" placeholder="Nom d'internaute" class="form-control input-md">
              </div>
            </div>
            <br>
            <!-- Text input-->
            <div class="form-group"> 
              <div class="col-md-5">
              <textarea class="form-control" id="textarea" name="comment" placeholder="Commentaire"></textarea>
              
            </div>
            </div>
            <br>
            

            <br>
            <!-- Button -->
            <button type="submit" class="btn btn-lg btn-outline-primary col-md-5"> Commenter </button>

            </fieldset>
            </form>

            </center>
            <small class="text-muted fw-light">
            <?php foreach($cmts as $cmt){ 
              echo "<hr>";
              echo $cmt->getNomInter() . " : ". $cmt->getComment();
              echo "<hr>";
              } ?>
            </small>
            <br><br>
          </div>
        </div>
      </div>



      
      
    </div>


    
  </main>

  <footer class="pt-4 my-md-5 pt-md-5 border-top">
    
  </footer>
</div>


    
  </body>


  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
  
</html>
