<?php
require_once 'ArticleManager.php';
$Manage_art = new ArticleManager();
$articles = $Manage_art->findAll();

require_once 'CategorieManager.php';
$Manage_cat = new CategorieManager();
$categories = $Manage_cat->findAll();

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>Pricing example · Bootstrap v5.1</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/pricing/">

    

    <!-- Bootstrap core CSS -->
<link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="pricing.css" rel="stylesheet">
  </head>
  <body>
    
<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
  <symbol id="check" viewBox="0 0 16 16">
    <title>Check</title>
    <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z"/>
  </symbol>
</svg>

<div class="container py-3">
<header>
    <?php include("nav.html") ?>
  </header>

  <main>
  <div class="row row-cols-1 row-cols-md-12 mb-3 text-center">



    <div class="col">
        <div class="card mb-8 rounded-3 shadow-sm">
          <div class="card-header py-3">
            <h4 class="my-0 fw-normal">Ajout Article </h4>
          </div>
          <div class="card-body">
            
            <center>
            <form class="form-horizontal" action="insert_art.php" method="POST">
            <fieldset>


            <!-- Text input-->
            <div class="form-group"> 
              <div class="col-md-5">
              <input id="textinput" name="nom_art" type="text" placeholder="Nom d'article" class="form-control input-md">
              </div>
            </div>
            <br>
            <!-- Text input-->
            <div class="form-group"> 
              <div class="col-md-5">
              <textarea class="form-control" id="textarea" name="description_art" placeholder="Description"></textarea>
              
            </div>
            </div>
            <br>
            <!-- Select Basic -->
            <div class="form-group">
              <div class="col-md-5">
                <select id="selectbasic" name="id_cat" class="form-control">
                <option value="">Choisisez une catégorie</option>
                <?php foreach($categories as $cat){  ?>
                  <option value="<?= $cat->getId(); ?>"><?= $cat->getId(); ?> : <?= $cat->getNom(); ?></option>
                <?php } ?>
                </select>
              </div>
            </div>

            <br>
            <!-- Button -->
            <button type="submit" class="btn btn-lg btn-outline-primary col-md-5">Save </button>

            </fieldset>
            </form>

            </center>
          </div>
        </div>
      </div>

    </div>

    <h2 class="display-6 text-center mb-4">Liste de Articles</h2>

    <div class="table-responsive">
      <table class="table text-center">
        <thead>
          <tr>
            <th style="width: 2%;">Id</th>
            <th style="width: 8%;">id Cat</th>
            <th style="width: 20%;">Nom art</th>
            <th style="width: 55%;">description art</th>
            <th style="width: 15%;">action</th>
          </tr>
        </thead>
        <?php foreach($articles as $art){ ?>
        <tbody>
        
          <tr>
          <th scope="row" class="text-start"> <?= $art->getId(); ?></th>
          <th scope="row" class="text-start"> <?= $art->getIdCat(); ?></th>
            <td> <?= $art->getNom(); ?></td>
            <td> <?= $art->getDescription(); ?></td>
            <td>
              <a href="delete_art.php?id_art=<?= $art->getId(); ?>">
								Supprimer
							</a>
              <br>
              <a href="edit_art.php?id_art=<?= $art->getId(); ?>">
								Modifier
							</a>
            </td>
          </tr>
          
        </tbody>
<?php } ?>
      </table>
    </div>
  </main>

  <footer class="pt-4 my-md-5 pt-md-5 border-top">
    
  </footer>
</div>


    
  </body>


  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
  
</html>


