<?php

require_once 'CommentManager.php';

if(!isset($_POST['nom_inter']) || empty($_POST['nom_inter'])){
	echo 'internaute';
}
if(!isset($_POST['comment']) || empty($_POST['comment'])){
	echo 'cmt';
}
else{
	// Je ne vérifie pas la description car elle est autorisée à être vide en base

	// Création de l'objet
	$Manage_cmt = new CommentManager();
	$Manage_cmt->setNomInter($_POST['nom_inter'])
		->setComment($_POST['comment'])
		->setIdArt($_POST['id_art']);
	// Sauvegarde
	if($Manage_cmt->save() > 0){
		header('Location:save.php'); 
	}
	else{
		echo "<p>Une erreur est survenue</p>";
	}
}