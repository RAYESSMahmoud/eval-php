<?php

require_once 'ArticleManager.php';

if(!isset($_POST['nom_art']) || empty($_POST['nom_art'])){
	echo 'Veuillez renseigner un article';
}
if(!isset($_POST['description_art']) || empty($_POST['description_art'])){
	echo 'Veuillez renseigner une Description';
}if(!isset($_POST['id_cat']) || empty($_POST['id_cat'])){
	echo 'Veuillez renseigner une Catégorie';
}
else{
	// Je ne vérifie pas la description car elle est autorisée à être vide en base

	// Création de l'objet
	$Manage_art = new ArticleManager();
	$Manage_art->setNom($_POST['nom_art'])
		->setDescription($_POST['description_art'])
		->setIdCat($_POST['id_cat']);
	// Sauvegarde
	if($Manage_art->save() > 0){
		header('Location:save.php'); 
	}
	else{
		echo "<p>Une erreur est survenue</p>";
	}
}